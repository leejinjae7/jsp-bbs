/**
 * 
 */

function infoConfirm() {
	if(document.reg_frm.userID.value.length == 0) {
		alert("아이디는 필수 사항입니다.");
		reg_frm.userID.focus();
		return;
	}
	
	if(document.reg_frm.userID.value.length < 4) {
		alert("아이디는 4글자 이상이어야 합니다.");
		reg_frm.userID.focus();
		return;
	}
	
	if(document.reg_frm.userPassword.value.length == 0) {
		alert("비밀번호는 필수사항입니다.");
		reg_frm.userPassword.focus();
		return;
	}
	
	if(document.reg_frm.userPassword.value != document.reg_frm.userPassword_check.value) {
		alert("비밀번호가 일치하지 않습니다.");
		reg_frm.userPassword.focus();
		return;
	}
	
	if(document.reg_frm.userName.value.length == 0) {
		alert("이름은 필수사항입니다.");
		reg_frm.userName.focus();
		return;
	}
	
	if(document.reg_frm.userEmail.value.length == 0) {
		alert("이메일은 필수사항입니다.");
		reg_frm.userEmail.focus();
		return;
	}
	
	document.reg_frm.submit();
}